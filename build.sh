#!/bin/sh
if [ ! -d target ]; then mkdir target; fi
tar --exclude='target' --exclude='.git' --exclude='*.tar.gz' --exclude='node_modules' --exclude='*.log' -cvzf target/issue-mash.0.0.3.tar.gz *
