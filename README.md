# Issue Mash

Issue Mash ranks issues from an Agile Board by pitting them against each other in a battle royale.

You can find it in the Agile drop-down.

(Requires JIRA Agile, obvs).
