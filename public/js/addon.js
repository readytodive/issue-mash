/* add-on script */
(function (_, $) {
    'use strict';

    var issues;

    function redrawBoardSelect(boards) {
        if (boards) {
            var boardOptions = boards.map(_.template('<option value="<%- id %>"><%- name %></option>')).join('\n');
            $('#board-id-select').html(boardOptions);
        }
    }

    function issueTable(issues) {
        var tableOpen = '<table class="aui">'
                + '         <thead> <tr> <th>Key</th> <th>Summary</th> </tr> </thead>'
                + '         <tbody>';
        var tableClose = '</tbody> </table>';
        var issueRow = _.template('<tr> <td data-key="<%- key %>"><%- key %></td> <td><%- summary %></td> </tr>');
        return tableOpen + issues.map(issueRow).join('\n') + tableClose;
    }

    function redrawIssueTable() {
        $('#issue-table').html(issueTable(_(issues).sortBy(function (issue) {
            return -issue.rating;
        })));
    }

    var pick = (function() {
        var l = 0;
        var r = 1;
        function next() {
            r++;
            if (l >= issues.length) {
                $('#aui-message-bar').html('');
                AJS.messages.success({title: 'Done!', body: 'Issues have been re-ranked according to your choices.', closeable: false});
                $('#issue-pair').html('<button id="save-ranking" class="aui-button aui-button-primary">Save new ranking</button>');
            } else if (r >= issues.length) {
                l++;
                r = l;
                next();
            } else {
                drawIssuePair(issues[l], issues[r]);
            }
        }

        return function pick(side) {
            var $winner = $('#' + side + '-issue');
            if ($winner.length === 0) return;
            var winnerKey = $winner.attr('data-key');
            var loserKey = $('#' + (side === 'left' ? 'right' : 'left') + '-issue').attr('data-key');
            var winner = _.find(issues, function (issue) { return issue.key === winnerKey; });
            var loser = _.find(issues, function (issue) { return issue.key === loserKey; });
            var winnerExpected = 1 / (1 + (Math.pow(10,(loser.rating - winner.rating) / 400)));
            var loserExpected = 1 / (1 + (Math.pow(10,(winner.rating - loser.rating) / 400)));
            var k = 30;
            winner.rating = Math.round(winner.rating + (k * (1 - winnerExpected)));
            loser.rating = Math.round(loser.rating + (k * (0 - loserExpected)));
            redrawIssueTable();
            next();
        }
    })();

    function drawIssuePair(left, right) {
        var issuePair = _.template('<span id="left-issue" data-key="<%= left.key %>"><%- left.summary %></span>'
                + '<span id="or"><b class="arrow">&larr;</b><span id="vs">VS</span><b class="arrow">&rarr;</b></span>'
                + '<span id="right-issue" data-key="<%= right.key %>"><%- right.summary %></span>');

        $('#issue-pair').html(issuePair({left: left, right: right}));
    }

    $(function () {
        var $auiPanel = $('.aui-page-panel.main-panel');
        $auiPanel.on('click', '#left-issue', function() { pick('left'); });
        $auiPanel.on('click', '#right-issue', function() { pick('right'); });

        AP.require(['request'], function (request) {

            request({
                url: '/rest/greenhopper/1.0/rapidview',
                success: function (response) {
                    redrawBoardSelect(JSON.parse(response).views);
                    $('#board-id-select').auiSelect2();
                }
            });

            $auiPanel.on('submit', '#board-select-form', function (e) {
                e.preventDefault();
                $('#board-select-form').find('.error').remove();

                var rankField;

                $('.button-spinner').addClass("aui-icon-wait");
                request({
                    url: '/rest/greenhopper/1.0/xboard/plan/backlog/data?rapidViewId=' +
                            encodeURIComponent($('#board-id-select').val()),

                    success: function (response) {
                        response = JSON.parse(response);
                        rankField = response.rankCustomFieldId;
                        issues = _.chain(response.issues).filter(function(issue) {
                            return +issue.status.statusCategory.id === 2;
                        }).shuffle().value();
                        _.each(issues, function (issue) {
                            issue.rating = 1000;
                        });

                        $('.button-spinner').removeClass("aui-icon-wait");
                        redrawIssueTable();

                        if (issues.length < 2) {
                            $('#issue-table').html('');
                            AJS.messages.error('#issue-table', {
                                title: 'Not enough issues!',
                                body:'Please choose a board with at least 2 "To Do" issues'
                            });
                        } else {
                            $('#board-select-form').remove();
                            $('.arena').removeClass('hidden');
                            drawIssuePair(issues[0], issues[1]);
                            $(document).whenIType('left').execute(_.partial(pick, 'left'));
                            $(document).whenIType('right').execute(_.partial(pick, 'right'));
                        }
                    },

                    error: function () {
                        $('.button-spinner').removeClass("aui-icon-wait");
                        $('#board-select-form').find('.field-group').eq(0).append('<div class="error hidden">We couldn\'t find a board with that ID</div>');
                    },

                    contentType: 'application/json'
                });

                function markSuccessfulRank(key) {
                    $('td[data-key=' + key + ']').append('<span class="progress tick">&#10004;</span>');
                }

                function markFailedRank(key) {
                    $('td[data-key=' + key + ']').append('<span class="progress cross">&#120299;</span>');
                }

                var rankInProgress = false;

                function rank(previousKey, keys, fieldId) {
                    if (keys.length < 1) {
                        rankInProgress = false;
                        return;
                    }
                    var key = keys.shift();
                    var data = {
                        issueKeys: [
                            key
                        ],
                        customFieldId: fieldId,
                        rankAfterKey: previousKey
                    };
                    console.debug(data);
                    request({
                        type: 'PUT',
                        url: '/rest/greenhopper/1.0/api/rank/after',
                        contentType: 'application/json',
                        success: function () {
                            markSuccessfulRank(key);
                            rank(key, keys, fieldId);
                        },
                        error: function () {
                            markFailedRank(key);
                            rankInProgress = false;
                        },
                        data: JSON.stringify(data)
                    });
                }

                $auiPanel.on('click', '#save-ranking', function() {
                    if (rankInProgress) return;
                    rankInProgress = true;
                    $("#issue-table").find('.progress').remove();
                    var sortedKeys = _.chain(issues).sortBy(function (issue) {
                        return -issue.rating;
                    }).pluck('key').value();
                    var firstKey = sortedKeys.shift();
                    markSuccessfulRank(firstKey);
                    rank(firstKey, sortedKeys, rankField);
                });

            });
        });
    });
})(_, jQuery);