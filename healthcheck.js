var moment = require('moment');
var filesize = require('filesize');
var fs = require('fs');
var pkg = JSON.parse(fs.readFileSync('./package.json'));

module.exports = function (req, res) {
    res.end(JSON.stringify(heartbeat()));
};

function heartbeat () {
    var mem = process.memoryUsage();
    var uptime = moment.duration(process.uptime(), 'seconds');

    return {
        pid: process.pid,
        environment: process.env.ZONE,
        name: pkg.name,
        version: pkg.version,
        uptime: uptime.humanize(),
        engine: process.version,
        modules: process.versions,
        arch: process.arch,
        platform: process.platform,
        memory: {
            app: filesize(mem.rss),
            v8: {
                heapTotal: filesize(mem.heapTotal),
                heapUsed: filesize(mem.heapUsed)
            }
        },
        status: 'Ok'
    }
}
